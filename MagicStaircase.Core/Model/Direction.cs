﻿namespace MagicStaircase.Core.Model
{
    public enum Direction
    {
        Up,
        Down
    }
}
